'use strict';

var socket = io();

function addBet(bet){
    $('#bets-history').prepend('<div class="jumbotron jumbotron-game-block">'+
        '<h3><a href="/user/'+bet._author.uid+'">'+bet._author.firstName+' '+bet._author.lastName+'</a> '+
        '<small>сделал ставку</small></h3>'+
        '<h4 class="lead">$'+bet.amount+'</h4></div>');
}

function updateBetsHistory(){
    $.ajax({
        url: '/api/game',
        dataType: 'json',
        method: 'get'
    }).done(function(data){
        var bets = data['bets'];
        var betsInfo = data['bets_info'];
        var game = data['game'];
        var left = data['left'];

        $('#bets-history').html('');
        if(betsInfo.count > 0){
            for(var i = 0; i < bets.length; i++){
                addBet(bets[i]);
            }
        }
        else $('#bets-history').html('<div class="alert alert-info">Пока на эту игру никто не ставил, Вы можете быть первыми.</div>');

        if(game.status == 0){
            $('#game-block').html('<h1 class="display-4 text-center">Ожидается начало игры </h1>'+
                '<p class="lead">Количество ставок: <b>'+betsInfo.count+'</b><br>'+
                'Общая сумма ставок: <b>$'+betsInfo.amount+'</b><br>'+
                'До начала игры осталось: <span id="game-left" style="font-weight: bold;">'+left+'</span></p>');
        }
        else if(game.status == 1){
            $('#game-block').html('<h1 class="display-4 text-center">Идет игра</h1>');
        }
        else
        {
            $('#game-block').html('game finished');
        }
    });
}

$(document).ready(function(){
    $('#bet').bind('click', function(){
        var bet = parseFloat($('#bet-amount').val());

        socket.emit('bet', bet);
    });
});

socket.on('update game', function(){
    updateBetsHistory();
});

socket.on('alert', function(message, type){
    alertify[type](message);
});

socket.on('update online', function(online){
    $('#online').text(online);
});

socket.on('update time', function(time, left){
    $('#time').text(time);
    $('#game-left').text(left);
});