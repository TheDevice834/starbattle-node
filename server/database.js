const mongoose = require('mongoose');

require('./models');

function Connection(config){
	this.User = mongoose.model('User');
	this.Game = mongoose.model('Game');
	this.Bet = mongoose.model('Bet');

    this.options = {
	    url: 'mongodb://'+config.database.user+':'+config.database.password+'@'+config.database.host+'/'+config.database.database+''
	}
    mongoose.connect(this.options.url, {
    	useMongoClient: true,
    	promiseLibrary: require('bluebird')
    });


}

Connection.prototype.sessionStore = function(session){
	let mongoStore = require('connect-mongo')(session);

	return new mongoStore(this.options);
}

module.exports = Connection;