const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const nunjucks = require('nunjucks');
const session = require('express-session');
const PassportClass = require('./strategies/Vk');
const utils = require('./utils');
const Connection = require('./database');

nunjucks.configure('./client/templates', {
    autoescape: true,
    express: app
}).addGlobal('dateTime', function(){
    return utils.dateTime();
}).addGlobal('online', function(){
    return online;
});

const config = utils.loadConfig();
const connection = new Connection(config);

const Passport = new PassportClass(config);
const passport = Passport.passport(connection.User);

let sessionMiddleware = session({
    key:                'express.sid',
    secret:             'dw62z8CmUt6N0PJSNxSy7bQG08xEVd',
    resave:             false,
    saveUninitialized:  false,
    store:              connection.sessionStore(session)
});

app.use(express.static('./client/public'));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({
    extended: true
}));
app.use(sessionMiddleware);
app.use(passport.initialize());
app.use(passport.session());


// App
let online = 0;
let time = utils.dateTime(true);
let left;
let currentGameId;

connection.Game.find({ status: 0 }).then((games) => {
    if(games.length < 1){
        let game = new connection.Game({
            status: 0
        });
        game.save().then((game) => {
            currentGameId = game._id;
            io.emit('update game');
        });
    }
    else
    {
        currentGameId = games[0]._id;
    }
});


let timer = setInterval(function(){
    time.seconds++;

    left = utils.nextGameTime(time.minutes, time.seconds);

    io.emit('update time', utils.dateTime(), utils.gameLeft(left));

    if(time.seconds >= 60){
        time.seconds = 0;
        time.minutes++;

        if(Number.isInteger(time.minutes / 5)){
            connection.Game.findById(currentGameId).then((game) => {
                if(game.status == 1){
                    console.log('finished');
                }
            });
        }

        if(Number.isInteger(time.minutes / 10)){
            connection.Game.findById(currentGameId).then((game) => {
                game.status = 1;
                game.save().then(() => {
                    io.emit('update game');
                });
            });
        }
    }
}, 1000);

app.get('/', (request, response) => {
    connection.Game.findById(currentGameId).then((game) => {
        connection.Bet.find({ _game: game._id }).populate('_author').then((bets) =>{
            response.render('main/index.html', {
                title: 'Star Battle Project',
                authUser: request.user,
                game: game,
                bets: {
                    list: bets,
                    info: {
                        count: bets.length,
                        amount: utils.fullAmount(bets)
                    }
                },
                left: utils.gameLeft(left)
            });
        });
    });
});
app.get('/about', (request, response) => {
    response.render('main/about.html', {
        title: 'О нас',
        authUser: request.user
    });
});
app.get('/user/:uid', (req, res) => {
    connection.User.findOne({ uid: req.params.uid }, 'uid firstName lastName avatar createdAt updatedAt', (err, user) => {
        if(err) throw err;
        if(user == null) return res.status(404).send('Sorry cant find that!');

        return res.render('users/view.html', {
            title: user.firstName+' '+user.lastName,
            authUser: req.user,
            user: user
        });
    });
});


app.get('/api/game', (request, response) => {
    connection.Game.findById(currentGameId).then((game) => {
        connection.Bet.find({ _game: game._id }).populate('_author').then((bets) =>{
            return response.json({
                game: game,
                bets: bets,
                bets_info: {
                    count: bets.length,
                    amount: utils.fullAmount(bets)
                },
                left: utils.gameLeft(left)
            });
        });
    });
});
app.get('/login', passport.authenticate('vkontakte'));
app.get('/login/callback',
    passport.authenticate('vkontakte', {
        successRedirect: '/',
        failureRedirect: '/'
    })
);
app.get('/logout', (request, response) => {
    request.logout();
    response.redirect('/');
});

io.use(function(socket, next){
    sessionMiddleware(socket.request, {}, next);
});

io.on('connection', (socket) => {
    online++;
    io.emit('update online', online);

    socket.on('bet', (bet) => {
        bet = parseFloat(bet);
        if(Number.isNaN(bet) || bet < 0.25) return socket.emit('alert', 'Ставки ниже $0.25 не принимаются', 'warning');

        if(user = socket.request.session.passport.user){
            connection.User.findById(user, (err, user) => {
                if(err) throw err;

                bet = new connection.Bet({
                    amount: bet,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                    _game: currentGameId,
                    _author: user._id,
                });

                bet.save(function(err){
                    if(err) throw err;

                    io.emit('update game');
                });
            });
        }
    });

    socket.on('disconnect', () => {
        online--;

        io.emit('update online', online);
    });
});

app.use(function(req, res, next){
    return res.sendStatus(404);
});

// Server
http.listen('80', '0.0.0.0', () => {
    console.log('Listening on 80...');
});
