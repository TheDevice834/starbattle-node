function Passport(config){
    this.config = config;
}

Passport.prototype.vkStrategy = function(User){
    var vkStrategy = require('passport-vkontakte').Strategy;
    return new vkStrategy(
        {
            clientID:       parseInt(this.config.vk.clientId),
            clientSecret:   this.config.vk.clientSecret,
            callbackURL:    this.config.vk.callback,
            profileFields:  this.config.vk.fields
        },
        function myVerifyCallbackFn(accessToken, refreshToken, params, profile, done) {
            User.findOne({ uid: profile.id }).then(function(user){
                if(user == null){
                    user = new User({
                        uid: profile.id,
                        firstName: profile.name.givenName,
                        lastName: profile.name.familyName,
                        avatar: profile.photos[1].value,
                        createdAt: new Date(),
                        updatedAt: new Date()
                    });
                    user.save(function(err){
                        if(err) throw err;
                        done(null, user);
                    });
                }
                else done(null, user);
            }).catch(done);
        }
    );
}

Passport.prototype.passport = function(User){
    var passportObj = require('passport');

    passportObj.use(this.vkStrategy(User));
    passportObj.serializeUser(function(user, done) {
        done(null, user.id);
    });
    passportObj.deserializeUser(function(id, done) {
        User.findById(id).then(function(user){
            done(null, user);
        }).catch(done);
    });
    return passportObj;
}

module.exports = Passport;
