const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
	uid: { type: Number, required: true },
	firstName: { type: String, required: true },
	lastName: { type: String, required: true },
	avatar: { type: String, required: true },
	createdAt: { type: Date, required: true },
	updatedAt: { type: Date, required: true },
});

const GameSchema = new Schema({
	status: { type: Number, required: true},
	winner: { type: Number }
});

const BetSchema = new Schema({
	amount: { type: Number, required: true },
	createdAt: { type: Date, required: true },
	updatedAt: { type: Date, required: true },
	_game: { type: String, required: true },
	_author: { type: String, ref: 'User' },
});

const User = mongoose.model('User', UserSchema);
const Game = mongoose.model('Game', GameSchema);
const Bet = mongoose.model('Bet', BetSchema);