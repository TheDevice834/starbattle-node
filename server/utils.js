const fs = require('fs');
const ini = require('ini');

var utils = {
    dateTime: function(onlyTime = false){
        let date = new Date();

        if(onlyTime){
            return {
                minutes: date.getMinutes(),
                seconds: date.getSeconds()
            };
        }

        let hour = date.getHours();
        hour = (hour < 10 ? "0" : "") + hour;
        let minute  = date.getMinutes();
        minute = (minute < 10 ? "0" : "") + minute;
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;
        let day  = date.getDate();
        day = (day < 10 ? "0" : "") + day;

        return day+"."+month+"."+year+", "+hour+":"+minute;
    },

    fullAmount: function(bets){
        let amount = 0;
        for(var i = 0; i < bets.length; i++){
            amount += bets[i].amount;
        }

        return amount;
    },

    fullTicketsCount: function(bets){
        let tickets = 0;

        for(var i = 0; i < bets.length; i++){
            tickets += bets[i].tickets;
        }

        return tickets;
    },

    nextGameTime: function(minutes, seconds){
        return ((((parseInt(minutes / 10)+1)*10)-minutes)*60)-seconds;
    },

    gameLeft: function(leftTime){
        let minutes = parseInt(leftTime / 60);
        let seconds = parseInt(leftTime % 60);
        seconds = (seconds < 10 ? "0" : "") + seconds;

        let left = minutes+':'+seconds;

        return left;
    },

    loadConfig: function(){
        var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));

        return config;
    },
};

module.exports = utils;
